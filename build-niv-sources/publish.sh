set -e

git submodule update --init --recursive

pushd output-channels/niv-sources/
  git fetch
  git pull origin main
popd

rsync --partial --progress --recursive --delete --checksum processing/niv-calling/nix output-channels/niv-sources/

pushd output-channels/niv-sources/
  git diff --submodule
  git add -A
  git commit -m "niv update" || true
  git status
  git push
popd

git add -A
git commit -m "niv update"

git push --recurse-submodules=check
